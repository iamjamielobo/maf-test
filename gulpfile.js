'use strict';

const GULP = require('gulp');
const UGLIFY = require('gulp-uglify');
const JSHINT = require('gulp-jshint');
const SASS = require('gulp-sass');
const SOURCE_MAPS = require('gulp-sourcemaps');
const CONCAT = require('gulp-concat');
const GUTIL = require('gulp-util');
const CSS_MINIFY = require('gulp-clean-css');
const AUTOPREFIXER = require('gulp-autoprefixer');

GULP.task('js', () => {
    return GULP
        .src('src/scripts/*.js')
        .pipe(SOURCE_MAPS.init())
        .pipe(CONCAT('App.js'))
        .pipe(GUTIL.env.type === 'production' ? UGLIFY() : GUTIL.noop())
        .pipe(SOURCE_MAPS.write())
        .pipe(JSHINT())
        .pipe(JSHINT.reporter('jshint-stylish'))
        .pipe(GULP.dest('public/scripts'));
});

GULP.task('sass', () => {
    return GULP
        .src('src/stylesheets/*.scss')
        .pipe(SOURCE_MAPS.init())
        .pipe(SASS())
        .pipe(GUTIL.env.type === 'production' ? CSS_MINIFY() : GUTIL.noop())
        .pipe(SOURCE_MAPS.write())
        .pipe(AUTOPREFIXER('last 3 versions', 'ie10', 'safari >= 9', 'iOS >= 7'))
        .pipe(CONCAT('App.css'))
        .pipe(GULP.dest('public/stylesheets'));
});

GULP.task('watch', () => {
    GULP.watch('src/scripts/**/*.js', ['js']);
    GULP.watch('src/stylesheets/**/*.scss', ['sass']);
});

GULP.task('build', ['js', 'sass']);

GULP.task('default', ['watch']);