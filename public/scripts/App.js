var dom = {
    events: {},
    container: {},
    button: {},
    cacheElements: function() {
        dom.body = $('body');
        dom.button.search = dom.body.find('.search');
        dom.container.form = dom.body.find('.form');
        dom.button.signin = dom.body.find('.sign-btn');
        dom.container.loading = dom.body.find('.loading-container');
        dom.container.signup = dom.body.find('.signup-block');
        dom.container.success = dom.body.find('.success-msg');
    },
    initEventListeners: function() {
        dom.button.search.click(function() {
            var $this = $(this);
            if (!$this.hasClass('open')) {
                $this.addClass('open');
            }
        });
        dom.button.signin.click(function() {
            var elArr = dom.container.form.find('input');
            var $this = $(this);
            var temp = [];
            var userName = '';

            $this.addClass('disabled');

            elArr.each(function(i, o) {
                var val = o.value;
                var el = $(o);
                if (!val.length) {
                    el.siblings('.error-msg').text('Required field');
                    el.closest('label').addClass('error');
                    temp.push(el);
                } else {
                    el.closest('label').removeClass('error');
                    if (el.attr('id') === 'inpUser') {
                        userName = el.val();
                    }
                }
            });

            if (!temp.length) {
                dom.container.loading.addClass('show');
                $.get("https://httpbin.org/get", function(data) {
                    var msg = 'Congratulations ' + userName + '!!! you have successfully logged in. Please enjoy shopping at Carrefour.';
                    dom.container.loading.removeClass('show');
                    dom.container.signup.addClass('hide');
                    dom.container.success.text(msg).addClass('show');
                });
            } else {
                $this.removeClass('disabled');
            }
        });
    }
};

$(document).ready(function() {
    dom.cacheElements();
    dom.initEventListeners();
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFwcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6IkFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciBkb20gPSB7XG4gICAgZXZlbnRzOiB7fSxcbiAgICBjb250YWluZXI6IHt9LFxuICAgIGJ1dHRvbjoge30sXG4gICAgY2FjaGVFbGVtZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgIGRvbS5ib2R5ID0gJCgnYm9keScpO1xuICAgICAgICBkb20uYnV0dG9uLnNlYXJjaCA9IGRvbS5ib2R5LmZpbmQoJy5zZWFyY2gnKTtcbiAgICAgICAgZG9tLmNvbnRhaW5lci5mb3JtID0gZG9tLmJvZHkuZmluZCgnLmZvcm0nKTtcbiAgICAgICAgZG9tLmJ1dHRvbi5zaWduaW4gPSBkb20uYm9keS5maW5kKCcuc2lnbi1idG4nKTtcbiAgICAgICAgZG9tLmNvbnRhaW5lci5sb2FkaW5nID0gZG9tLmJvZHkuZmluZCgnLmxvYWRpbmctY29udGFpbmVyJyk7XG4gICAgICAgIGRvbS5jb250YWluZXIuc2lnbnVwID0gZG9tLmJvZHkuZmluZCgnLnNpZ251cC1ibG9jaycpO1xuICAgICAgICBkb20uY29udGFpbmVyLnN1Y2Nlc3MgPSBkb20uYm9keS5maW5kKCcuc3VjY2Vzcy1tc2cnKTtcbiAgICB9LFxuICAgIGluaXRFdmVudExpc3RlbmVyczogZnVuY3Rpb24oKSB7XG4gICAgICAgIGRvbS5idXR0b24uc2VhcmNoLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICAgICAgICAgIGlmICghJHRoaXMuaGFzQ2xhc3MoJ29wZW4nKSkge1xuICAgICAgICAgICAgICAgICR0aGlzLmFkZENsYXNzKCdvcGVuJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBkb20uYnV0dG9uLnNpZ25pbi5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBlbEFyciA9IGRvbS5jb250YWluZXIuZm9ybS5maW5kKCdpbnB1dCcpO1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICAgICAgICAgIHZhciB0ZW1wID0gW107XG4gICAgICAgICAgICB2YXIgdXNlck5hbWUgPSAnJztcblxuICAgICAgICAgICAgJHRoaXMuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cbiAgICAgICAgICAgIGVsQXJyLmVhY2goZnVuY3Rpb24oaSwgbykge1xuICAgICAgICAgICAgICAgIHZhciB2YWwgPSBvLnZhbHVlO1xuICAgICAgICAgICAgICAgIHZhciBlbCA9ICQobyk7XG4gICAgICAgICAgICAgICAgaWYgKCF2YWwubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIGVsLnNpYmxpbmdzKCcuZXJyb3ItbXNnJykudGV4dCgnUmVxdWlyZWQgZmllbGQnKTtcbiAgICAgICAgICAgICAgICAgICAgZWwuY2xvc2VzdCgnbGFiZWwnKS5hZGRDbGFzcygnZXJyb3InKTtcbiAgICAgICAgICAgICAgICAgICAgdGVtcC5wdXNoKGVsKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBlbC5jbG9zZXN0KCdsYWJlbCcpLnJlbW92ZUNsYXNzKCdlcnJvcicpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZWwuYXR0cignaWQnKSA9PT0gJ2lucFVzZXInKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyTmFtZSA9IGVsLnZhbCgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGlmICghdGVtcC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBkb20uY29udGFpbmVyLmxvYWRpbmcuYWRkQ2xhc3MoJ3Nob3cnKTtcbiAgICAgICAgICAgICAgICAkLmdldChcImh0dHBzOi8vaHR0cGJpbi5vcmcvZ2V0XCIsIGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIG1zZyA9ICdDb25ncmF0dWxhdGlvbnMgJyArIHVzZXJOYW1lICsgJyEhISB5b3UgaGF2ZSBzdWNjZXNzZnVsbHkgbG9nZ2VkIGluLiBQbGVhc2UgZW5qb3kgc2hvcHBpbmcgYXQgQ2FycmVmb3VyLic7XG4gICAgICAgICAgICAgICAgICAgIGRvbS5jb250YWluZXIubG9hZGluZy5yZW1vdmVDbGFzcygnc2hvdycpO1xuICAgICAgICAgICAgICAgICAgICBkb20uY29udGFpbmVyLnNpZ251cC5hZGRDbGFzcygnaGlkZScpO1xuICAgICAgICAgICAgICAgICAgICBkb20uY29udGFpbmVyLnN1Y2Nlc3MudGV4dChtc2cpLmFkZENsYXNzKCdzaG93Jyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICR0aGlzLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG59O1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICBkb20uY2FjaGVFbGVtZW50cygpO1xuICAgIGRvbS5pbml0RXZlbnRMaXN0ZW5lcnMoKTtcbn0pOyJdfQ==
