var dom = {
    events: {},
    container: {},
    button: {},
    cacheElements: function() {
        dom.body = $('body');
        dom.button.search = dom.body.find('.search');
        dom.container.form = dom.body.find('.form');
        dom.button.signin = dom.body.find('.sign-btn');
        dom.container.loading = dom.body.find('.loading-container');
        dom.container.signup = dom.body.find('.signup-block');
        dom.container.success = dom.body.find('.success-msg');
    },
    initEventListeners: function() {
        dom.button.search.click(function() {
            var $this = $(this);
            if (!$this.hasClass('open')) {
                $this.addClass('open');
            }
        });
        dom.button.signin.click(function() {
            var elArr = dom.container.form.find('input');
            var $this = $(this);
            var temp = [];
            var userName = '';

            $this.addClass('disabled');

            elArr.each(function(i, o) {
                var val = o.value;
                var el = $(o);
                if (!val.length) {
                    el.siblings('.error-msg').text('Required field');
                    el.closest('label').addClass('error');
                    temp.push(el);
                } else {
                    el.closest('label').removeClass('error');
                    if (el.attr('id') === 'inpUser') {
                        userName = el.val();
                    }
                }
            });

            if (!temp.length) {
                dom.container.loading.addClass('show');
                $.get("https://httpbin.org/get", function(data) {
                    var msg = 'Congratulations ' + userName + '!!! you have successfully logged in. Please enjoy shopping at Carrefour.';
                    dom.container.loading.removeClass('show');
                    dom.container.signup.addClass('hide');
                    dom.container.success.text(msg).addClass('show');
                });
            } else {
                $this.removeClass('disabled');
            }
        });
    }
};

$(document).ready(function() {
    dom.cacheElements();
    dom.initEventListeners();
});